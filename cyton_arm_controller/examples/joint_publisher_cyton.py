#!/usr/bin/env python
# script to receive control messages from ROS Arm navigation and pass it on to the 
# harware manager  nodes( dynamixel controllers)

import roslib
roslib.load_manifest('cyton_arm_controller')
import time
import rospy
import std_msgs
import sensor_msgs
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64

joint_names = ( 'shoulder_roll_controller',
		'shoulder_pitch_controller',
		'elbow_roll_controller',
		'elbow_pitch_controller',
		'wrist_roll_controller',
		'wrist_pitch_controller',
		'wrist_yaw_controller',
		
)	

def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)

def callback(data):
	j1_index= data.name.index('joint1')
	j2_index= data.name.index('joint2')
	j3_index= data.name.index('joint3')
	j4_index= data.name.index('joint4')
	j5_index= data.name.index('joint5')
	j6_index= data.name.index('joint6')
	j7_index= data.name.index('joint7')
	#if 'joint1' in data.name:
	#	print 	
	joint_commands = [0,0,0,0,0,0,0]
	shoulder_roll=data.position[j1_index]
	shoulder_roll_t=translate(shoulder_roll,5.27,10.43,-2.7,2.7)
	joint_commands[0]=shoulder_roll_t

	shoulder_pitch=data.position[j2_index]
	shoulder_pitch_t=translate(shoulder_pitch,4.72,7.85,-1.5,1.5)
	joint_commands[1]=shoulder_pitch_t

	elbow_roll=data.position[j3_index]
	elbow_roll_t=translate(elbow_roll,0.49,5.78,-2.7,2.7)
	joint_commands[2]=elbow_roll_t

	elbow_pitch=data.position[j4_index]
	elbow_pitch_t=translate(elbow_pitch,4.72,7.85,-1.5,1.5)
	joint_commands[3]=elbow_pitch_t

	wrist_roll=data.position[j5_index]
	wrist_roll_t=translate(wrist_roll,0.28,5.85,-2.7,2.7)
	joint_commands[4]=wrist_roll_t

	wrist_pitch=data.position[j6_index]
	wrist_pitch_t=translate(wrist_pitch,4.72,7.85,-1.5,1.5)
	joint_commands[5]=wrist_pitch_t

	wrist_yaw=data.position[j7_index]
	wrist_yaw_t=translate(wrist_yaw,4.72,7.85,-1.5,1.5)
	joint_commands[6]=wrist_yaw_t

	pubs = [rospy.Publisher(name + '/command', Float64) for name in joint_names]
	for i in range(len(pubs)):
		pubs[i].publish(joint_commands[i])
	
	
def echojs():
	rospy.init_node('Joint_states_mapper', anonymous=True)
	rospy.Subscriber('joint_states',JointState,callback)
	rospy.spin()


if __name__ == '__main__':
	try:
		echojs()		
		print "hello"
		
	except rospy.ROSInterruptException: pass
