GETTING STARTED
---------------

The ROS packages associated with Cyton are:

cyton_arm_navigation : the ROS arm navigation package generated for cyton

cyton_arm_controller : the package for interfacing with the cyton hardware

cyton_description    : contains the URDF model of the cyton arm.

INSTALLATION
------------

clone the repo into your ROS workspace
then enter the directories of the above 3 packages 
now make them using the command  "rosmake"

BRINGING UP CYTON IN RVIZ & CONTROLLING THE HARDWARE
-------------------------


BRING UP CYTON USING THE FOLLOWING COMMAND:

	roslaunch cyton_arm_navigation  cyton_bringup.launch 

NOW LAUNCH THE NODES FOR CONTROLLING THE HARDWARE USING THE FOLLOWING COMMAND:
		 
	roslaunch cyton_arm_controller cyton_hardware.launch
